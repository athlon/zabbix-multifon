# Zabbix шаблон для Megafon Мультифон

## Файлы репозитория

|  Имя файла | Описание   |
| ------------ | ------------ |
| megafon.get.py  | Скрипт получения данных из API   |
| megafon.numbers.py  | Скрипт автообнаружения номеров и паролей к API Megafon  |
|  multifon-params.conf | Параметры Zabbix агента   |
| multifon_template.xml  |  Шаблон Zabbix 3.4  |

## Зависимости Python3
- json
- requests
- xml.dom.minidom

## Установка
### Установка зависимостей для python
```bash
~# pip install json requests xml.dom.minidom
```
### Дальнейшая настройка
- Python скрипты копируем в **/usr/local/scripts** и даем скриптам право запуска (**chmod +x script_name.py**).
- Копируем содержимое файла multifon-params.conf в файл конфигурации zabbix-agent, и перезапускаем агент.
- Заполняем номерами массив **numbers** в скрипте **megafon.numbers.py**.
- Добавляем шаблон multifon_template.xml в Zabbix
- Навешиваем шаблон на хост, на котором производилась настройка